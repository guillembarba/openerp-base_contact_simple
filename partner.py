##############################################################################
#
# Copyright (c) 2012 Guillem Barba Domingo
#                         All Rights Reserved.
#                         http://www.guillem.alcarrer.net
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from osv import osv, fields
from tools.translate import _
import netsvc


def update_replicated_fields(self, cr, uid, new_vals, replicate_ids_by_table,
        context):
    for table in new_vals.keys():
        if not replicate_ids_by_table[table]:
            netsvc.Logger().notifyChannel(self._name, netsvc.LOG_ERROR,
                    "update_replicated_fields(): Table in 'new_vals' not in "
                    "'repicate_ids_by_table': %s" % table)
            continue
        if len(new_vals[table]) == 1:
            fieldname = new_vals[table].keys()[0]
            netsvc.Logger().notifyChannel(self._name, netsvc.LOG_DEBUG,
                    "Updating %s setting field %s to %s for IDS "
                    "%s" % (table, fieldname, new_vals[table][fieldname],
                            replicate_ids_by_table[table]))
            cr.execute("UPDATE %s SET %s = %%s WHERE id in %%s"
                            % (table, fieldname),
                    (new_vals[table][fieldname],
                            tuple(replicate_ids_by_table[table])))
            continue
        netsvc.Logger().notifyChannel(self._name, netsvc.LOG_DEBUG,
                "Updating %s setting fields %s to values %s for IDS "
                "%s" % (table, ", ".join(new_vals[table].keys()),
                        tuple(new_vals[table].values()),
                        tuple(replicate_ids_by_table[table])))
        cr.execute("UPDATE %s SET (%s) = %%s WHERE id in %%s"
                        % (table, ", ".join(new_vals[table].keys())),
                (tuple(new_vals[table].values()),
                        tuple(replicate_ids_by_table[table])))
    return True


class res_partner(osv.osv):
    _inherit = 'res.partner'

    _columns = {
        'is_simple': fields.boolean("Is Simple?", required=True,
                help="Simple Partners normally relates to a Fisically Person "
                "and it must to have one and only one Address, Job and "
                "Contact."),
        }

    _defaults = {
        'is_simple': lambda self, cr, uid, context: context.get('is_simple'),
        }

    # res.partner
    def _check_is_simple(self, cr, uid, ids, context=None):
        for partner in self.browse(cr, uid, ids, context):
            if not partner.is_simple:
                continue
            if len(partner.address) != 1:
                raise osv.except_osv(_("Invalid Simple Partner"),
                        _("The Partner '%(partner_name)s' (ID:%(partner_id)s) "
                            "is a Simple Contact but it has %(n_address)s "
                            "Addresses.\nSimple Partners must to have one and "
                            "only one Address.") % {
                                'partner_name': partner.name,
                                'partner_id': partner.id,
                                'n_address': len(partner.address),
                                })
            if len(partner.address[0].job_ids) != 1:
                raise osv.except_osv(_("Invalid Simple Partner"),
                        _("The Partner '%(partner_name)s' (ID:%(partner_id)s) "
                            "is a Simple Contact but it has %(n_jobs)s "
                            "Jobs.\nSimple Partners must to have one and only "
                            "one Job.") % {
                                'partner_name': partner.name,
                                'partner_id': partner.id,
                                'n_jobs': len(partner.job_ids),
                                })
        return True

    _constraints = [
        (_check_is_simple, '', ['is_simple', 'address']),
        ]

    # res.partner
    def onchange_name_is_simple(self, cr, uid, ids, name, is_simple, context):
        if not is_simple:
            return {}
        if not ids:
            return {
                'warning': {
                    'title': _("Creating New Simple Partner"),
                    'message': _("You are trying to create a new Simple "
                        "Partner, but here it's not the most easiest way.\n"
                        "It's strongly recommended to use the 'Create Partner'"
                        " wizard."),
                    },
                }
        if not name:
            return {}
        return {
            'warning': {
                'title': _("Changing Simple Partner Name"),
                'message': _("You are trying to change the name of a "
                    "Simple Partner. It's not recommended because it "
                    "couldn't be correctly replicated in Contact's "
                    "fields. So, it won't change the name of contact.\n"
                    "Please, change the name from the Contact's form."),
                },
            }

    # res.partner
    def _replicated_fields(self, context):
        return {
            # It has an special treatment. Is ignored when changed in Partner
            # or Address (a warning message is launched to user in on_change
            # event), and is 'replicated' only from contact change (because
            # it has name separated in different fields)
            #'name': ['res_partner', 'res_partner_address',
            #        'res_partner_contact'],
            'first_name': ['res_partner_contact', ],
            'name': ['res_partner_contact', ],

            # Three diferent 'domain' of 'res.partner.title'
            #'title': ['res_partner', 'res_partner_address',
            #        'res_partner_contact'],

            # TODO: vat in contact is defined in another module
            #'vat': ['res_partner', 'res_partner_contact'],

            # But with different size. in partner 64, in contact 120
            'website': ['res_partner', 'res_partner_contact'],
            # TODO: In Contact is translatable. drop it
            'comment': ['res_partner', 'res_partner_contact'],
            'active': ['res_partner', 'res_partner_address',
                    'res_partner_contact'],
            # TODO: Contact must to have 'company_id'
            'company_id': ['res_partner', 'res_partner_address'],
            'email': ['res_partner_address', 'res_partner_contact'],

            # TODO: in job is replace here by related to contact's mobile, but
            #       profile_educa adds 'phone' field to contact
            #'phone': ['res_partner_address', 'res_partner_job',
            #        'res_partner_contact'],
            'mobile': ['res_partner_address', 'res_partner_contact'],

            # Field removed from all Job's views => only in address
            #'fax': ['res_partner_address', 'res_partner_job'],

            # In 'job', is a related to contact added in training and
            # in address in never shown in a view #WTF!
            #'birthdate': ['res_partner_address', 'res_partner_job',
            #        'res_partner_contact'],

            # Different meanings
            #'function': ['res_partner_address', 'res_partner_job'],
            }

    # res.partner
    def _my_replicated_fields(self, context):
        all_rfields = self._replicated_fields(context)
        return dict((a, all_rfields[a]) for a in all_rfields
                if 'res_partner' in all_rfields[a])

    # res.partner
    def write(self, cr, uid, ids, vals, context=None):
        res = super(res_partner, self).write(cr, uid, ids, vals, context)
        new_vals = self._prepare_replicated_vals(cr, uid, vals, context)
        if not new_vals:
            return res
        simple_ids = self.search(cr, uid, [
                ('id', 'in', ids),
                ('is_simple', '=', True),
                ], context=context, limit=False)
        # When active is set False, here doesn't found any Partner
        if not simple_ids:
            return res
        ids_by_table = {}
        if 'res_partner_address' in new_vals.keys():
            ids_by_table['res_partner_address'] = self.pool\
                    .get('res.partner.address').search(cr, uid, [
                        ('partner_id', 'in', simple_ids),
                        ], context=context, limit=False)
            netsvc.Logger().notifyChannel(self._name, netsvc.LOG_DEBUG,
                    "Get Address IDS from Partner IDS (%s): %s"
                    % (ids, ids_by_table['res_partner_address']))
        if ('res_partner_job' in new_vals.keys() or
                'res_partner_contact' in new_vals.keys()):
            ids_by_table['res_partner_job'] = self.pool\
                    .get('res.partner.job').search(cr, uid, [
                        ('name', 'in', simple_ids),
                        ], context=context, limit=False)
            netsvc.Logger().notifyChannel(self._name, netsvc.LOG_DEBUG,
                    "Get Job IDS from Partner IDS (%s): %s"
                    % (ids, ids_by_table['res_partner_job']))
        if 'res_partner_contact' in new_vals.keys():
            new_context = context.copy()
            new_context['simple_job_ids'] = ids_by_table['res_partner_job']
            ids_by_table['res_partner_contact'] = \
                    self.pool.get('res.partner.contact').search(cr, uid, [
                            ('is_simple', '=', True),
                            ('job_ids', 'in', ids_by_table['res_partner_job']),
                            ], context=new_context, limit=False)
            netsvc.Logger().notifyChannel(self._name, netsvc.LOG_DEBUG,
                    "Get Contact IDS from Partner IDS (%s): %s"
                    % (ids, ids_by_table['res_partner_contact']))
        netsvc.Logger().notifyChannel(self._name, netsvc.LOG_DEBUG,
                "calling update_replicated_fields(new_vals=%s, "
                        "ids_by_table=%s)" % (new_vals, ids_by_table))
        update_replicated_fields(self, cr, uid, new_vals, ids_by_table,
                context)
        return res

    # res.partner
    def _prepare_replicated_vals(self, cr, uid, vals, context):
        my_rfields = self._my_replicated_fields(context)
        mod_rfields = set(vals.keys()) & set(my_rfields.keys())
        netsvc.Logger().notifyChannel(self._name, netsvc.LOG_DEBUG,
                "fields in replicated and in vals : %s" % mod_rfields)
        print "vals: ", vals
        print "mod_rfields: ", mod_rfields
        if not mod_rfields:
            return {}
        mod_rtables = set(['res_partner_address', 'res_partner_job',
                    'res_partner_contact'])
        new_vals = {}
        for mod_rfieldname in mod_rfields:
            newval = vals[mod_rfieldname]
            column_type = self._columns[mod_rfieldname]
            if isinstance(column_type, fields.char) and newval is False:
                newval = ''
            elif isinstance(column_type, fields.boolean):
                newval = bool(vals[mod_rfieldname])
            for table in mod_rtables:
                if table in my_rfields[mod_rfieldname]:
                    print "settings %s['%s']=%s" % (table, mod_rfieldname, vals[mod_rfieldname])
                    new_vals.setdefault(table, {})[mod_rfieldname] = newval
        return new_vals

    # res.partner
    def update_replicated_fields(self, cr, uid, new_vals, 
            replicate_ids_by_table, context):
        return update_replicated_fields(self, cr, uid, new_vals,
                replicate_ids_by_table, context)
res_partner()


class res_partner_address(osv.osv):
    _inherit = 'res.partner.address'

    # res.partner.address
    def _check_simple_partner(self, cr, uid, ids, context=None):
        for address in self.browse(cr, uid, ids, context):
            if not address.partner_id or not address.partner_id.is_simple:
                continue
            n_addresses = self.search(cr, uid, [
                        ('partner_id', '=', address.partner_id.id),
                        ('id', '!=', address.id),
                        ], count=True, context=context)
            if n_addresses:
                raise osv.except_osv(_("Invalid Simple Partner"),
                        _("The Address with ID %(address_id)s is related to "
                          "partner '%(partner_name)s' (ID:%(partner_id)s) "
                          "which is a Simple Contact, but it has "
                          "%(n_address)s Addresses.\nSimple Partners must to "
                          "have one and only one Address.") % {
                              'address_id': address.id,
                              'partner_name': address.partner_id.name,
                              'partner_id': address.partner_id.id,
                              'n_address': n_addresses + 1,
                              })
            if len(address.job_ids) != 1:
                raise osv.except_osv(_("Invalid Simple Partner"),
                        _("The Address with ID %(address_id)s is related to "
                          "partner '%(partner_name)s' (ID:%(partner_id)s) "
                          "which is a Simple Contact, but it has  %(n_jobs)s "
                          "Jobs.\nSimple Partners must to have one and only "
                          "one Job.") % {
                              'address_id': address.id,
                              'partner_name': address.partner_id.name,
                              'partner_id': address.partner_id.id,
                              'n_jobs': len(address.job_ids),
                          })
        return True
    
    _constraints = [
        (_check_simple_partner, '', ['partner_id', 'job_ids']),
        ]

    # res.partner.address
    def onchange_job_ids(self, cr, uid, ids, is_simple, job_ids, context):
        if not is_simple:
            return {}
        return {
            'warning': {
                'title': _("Modifying Simple Partner Contacts"),
                'message': _("You are trying to modify Contact's data of a "
                    "Simple Partner from Partner's form.It's not recommended "
                    "because it couldn't be correctly replicated.\n"
                    "Please, cancel the modifications and change this values "
                    "in the Contact's form."),
                },
            }

    # res.partner.address
    def _my_replicated_fields(self, context):
        all_rfields = self.pool.get('res.partner')._replicated_fields(context)
        return dict((a, all_rfields[a]) for a in all_rfields
                if 'res_partner_address' in all_rfields[a])

    # res.partner.address
    def write(self, cr, uid, ids, vals, context=None):
        res = super(res_partner_address, self).write(cr, uid, ids, vals,
                context)
        my_rfields = self._my_replicated_fields(context)
        mod_rfields = set(vals.keys()) & set(my_rfields.keys())
        netsvc.Logger().notifyChannel(self._name, netsvc.LOG_DEBUG,
                "fields in replicated and in vals : %s" % mod_rfields)
        if not mod_rfields:
            return res
        simple_address_list = [a for a in self.browse(cr, uid, ids, context)
                if a.partner_id and a.partner_id.is_simple]
        if not simple_address_list:
            return res
        mod_rtables = set(['res_partner', 'res_partner_job',
                    'res_partner_contact'])
        new_vals = {}
        for mod_rfieldname in mod_rfields:
            newval = vals[mod_rfieldname]
            column_type = self._columns[mod_rfieldname]
            if isinstance(column_type, fields.char) and newval is False:
                newval = ''
            elif isinstance(column_type, fields.boolean):
                newval = bool(vals[mod_rfieldname])
            for table in mod_rtables:
                if table in my_rfields[mod_rfieldname]:
                    new_vals.setdefault(table, {})[mod_rfieldname] = newval
        ids_by_table = {}
        simple_partner_ids = [a.partner_id.id for a in simple_address_list]
        simple_address_ids = [a.id for a in simple_address_list]
        ids_by_table['res_partner'] = simple_partner_ids
        if ('res_partner_job' in new_vals.keys() or
                'res_partner_contact' in new_vals.keys()):
            ids_by_table['res_partner_job'] = self.pool\
                    .get('res.partner.job').search(cr, uid, [
                        #('name', 'in', simple_partner_ids),
                        ('address_id', 'in', simple_address_ids),
                        ], context=context, limit=False)
            netsvc.Logger().notifyChannel(self._name, netsvc.LOG_DEBUG,
                    "Get Job IDS from Address IDS (%s): %s"
                    % (simple_address_ids, ids_by_table['res_partner_job']))
        if 'res_partner_contact' in new_vals.keys():
            new_context = context.copy()
            new_context['simple_job_ids'] = ids_by_table['res_partner_job']
            ids_by_table['res_partner_contact'] = \
                    self.pool.get('res.partner.contact').search(cr, uid, [
                            ('is_simple', '=', True),
                            ('job_ids', 'in', ids_by_table['res_partner_job']),
                            ], context=new_context, limit=False)
            netsvc.Logger().notifyChannel(self._name, netsvc.LOG_DEBUG,
                    "Get Contact IDS from Partner IDS (%s): %s"
                    % (ids, ids_by_table['res_partner_contact']))
        netsvc.Logger().notifyChannel(self._name, netsvc.LOG_DEBUG,
                "calling update_replicated_fields(new_vals=%s, "
                        "ids_by_table=%s)" % (new_vals, ids_by_table))
        update_replicated_fields(self, cr, uid, new_vals, ids_by_table,
                context)
        return res

res_partner_address()


class res_partner_job(osv.osv):
    _inherit = 'res.partner.job'

    _columns = {
        'email': fields.related('contact_id', 'email', type='char',
                string='E-Mail', size=240, readonly=True),
        'phone': fields.related('contact_id', 'mobile', type='char',
                string='Mobile', size=64, readonly=True),
    }

    # res.partner.job
    def _check_simple_partner(self, cr, uid, ids, context=None):
        for job in self.browse(cr, uid, ids, context):
            if (not job.address_id.partner_id or
                    not job.address_id.partner_id.is_simple):
                continue
            n_jobs = self.search(cr, uid, [
                        ('address_id.partner_id', '=',
                                job.address_id.partner_id.id),
                        ('id', '!=', job.id),
                        ], count=True, context=context)
            if n_jobs:
                raise osv.except_osv(_("Invalid Simple Partner"),
                        _("The Job ID %(job_id)s is related to partner "
                          "'%(partner_name)s' (ID:%(partner_id)s) "
                          "which is a Simple Contact, but it has "
                          "%(n_jobs)s Jobs.\nSimple Partners must to "
                          "have one and only one Job.") % {
                              'job_id': job.id,
                              'partner_name': job.address_id.partner_id.name,
                              'partner_id': job.address_id.partner_id.id,
                              'n_jobs': n_jobs + 1,
                              })
        return True
    
    _constraints = [
        (_check_simple_partner, '', ['address_id']),
        ]

    # res.partner.job
    def _my_replicated_fields(self, context):
        all_rfields = self.pool.get('res.partner')._replicated_fields(context)
        return dict((a, all_rfields[a]) for a in all_rfields
                if 'res_partner_job' in all_rfields[a])

res_partner_job()

class res_partner_contact(osv.osv):
    _inherit = 'res.partner.contact'

    # res.partner.contact
    def _calc_is_simple(self, cr, uid, ids, fieldname, args, context=None):
        res = {}.fromkeys(ids, False)
        for contact in self.browse(cr, uid, ids, context):
            res[contact.id] = (len(contact.job_ids) == 1 and
                    contact.job_ids[0].name.is_simple or False)
        return res

    # res.partner.contact
    def _search_is_simple(self, cr, uid, obj, name, args, context):
        netsvc.Logger().notifyChannel(self._name, netsvc.LOG_DEBUG,
                "_search_is_simple: name=%s, obj=%s, args=%s"
                        % (name, obj, args))
        is_simple = None
        if args and args[0][0] == 'is_simple':
            if isinstance(args[0][2], (str, unicode)):
                is_simple = eval(args[0][2])
            else:
                is_simple = bool(args[0][2])
            if args[0][1] != '=':
                is_simple = not is_simple
        if is_simple is not None:
            extra_query = ""
            extra_params = ()
            if context and context.get('simple_job_ids'):
                extra_query += " j.id IN %s AND "
                extra_params += (tuple(context['simple_job_ids']),)
            cr.execute("""SELECT j.contact_id
                    FROM res_partner_job j
                        LEFT JOIN res_partner_address a ON (a.id=j.address_id)
                        LEFT JOIN res_partner p ON (p.id=a.partner_id)
                    WHERE """ + extra_query + "p.is_simple IS True",
                    extra_params)
            simple_contact_ids = [r[0] for r in cr.fetchall()]
            op = is_simple and 'in' or 'not in'
            netsvc.Logger().notifyChannel(self._name, netsvc.LOG_DEBUG,
                    "returning domain: [('id', %s, %s)]\nquery:%s" % (op,
                            simple_contact_ids, cr._obj.query))
            return [('id', op, simple_contact_ids)]
        return []

    _columns = {
        'is_simple': fields.function(_calc_is_simple, method=True,
                type='boolean', string='Is Simple?',
                fnct_search=_search_is_simple),
        'comment': fields.text('Notes', translate=False),
        }

    # res.partner.contact
    def _check_simple_contact(self, cr, uid, ids, context=None):
        for contact in self.browse(cr, uid, ids, context):
            is_simple = any([j.name.is_simple for j in contact.job_ids])
            if not is_simple:
                continue
            if len(contact.job_ids) != 1:
                contact_name = contact.name
                if contact.first_name:
                    contact_name = contact.first_name + " " + contact_name
                raise osv.except_osv(_("Invalid Simple Contact"),
                        _("The Contact '%(contact_name)s' (ID:%(contact_id)s) "
                          "is related to a Partner which is a Simple Contact, "
                          "but it has %(n_jobs)s.\nSimple Contacts must to "
                          "have one and only one Job.") % {
                              'contact_name': contact_name,
                              'contact_id': contact.id,
                              'n_jobs': len(contact.job_ids),
                          })
        return True
    
    _constraints = [
        (_check_simple_contact, '', ['job_ids']),
        ]

    # res.partner.contact
    def _my_replicated_fields(self, context):
        all_rfields = self.pool.get('res.partner')._replicated_fields(context)
        return dict((a, all_rfields[a]) for a in all_rfields
                if 'res_partner_contact' in all_rfields[a])

    # res.partner.contact
    def write(self, cr, uid, ids, vals, context=None):
        res = super(res_partner_contact, self).write(cr, uid, ids, vals,
                context)
        new_vals = self._prepare_replicated_vals(cr, uid, vals, context)
        if not new_vals:
            return res
        simple_contact_ids = self.search(cr, uid, [
                    ('id', 'in', ids),
                    ('is_simple', '=', True),
                    ], context=context, limit=False)
        netsvc.Logger().notifyChannel(self._name, netsvc.LOG_DEBUG,
                "simple_contact_ids=%s" % simple_contact_ids)
        if not simple_contact_ids:
            return res
        ids_by_table = {}
        if ('res_partner_job' in new_vals.keys() or
                'res_partner_address' in new_vals.keys() or
                'res_partner' in new_vals.keys()):
            ids_by_table['res_partner_job'] = self.pool.get('res.partner.job')\
                    .search(cr, uid, [
                            ('contact_id', 'in', simple_contact_ids),
                            ], context=context, limit=False)
            netsvc.Logger().notifyChannel(self._name, netsvc.LOG_DEBUG,
                    "Get Job IDS from Contact IDS (%s): %s"
                    % (simple_contact_ids, ids_by_table['res_partner_job']))
        if ('res_partner_address' in new_vals.keys() or
                'res_partner' in new_vals.keys()):
            ids_by_table['res_partner_address'] = \
                    self.pool.get('res.partner.address').search(cr, uid, [
                            ('job_ids', 'in',
                                    ids_by_table['res_partner_job']),
                            ], context=context, limit=False)
            netsvc.Logger().notifyChannel(self._name, netsvc.LOG_DEBUG,
                    "Get Address IDS from Job IDS (%s): %s"
                    % (ids_by_table['res_partner_job'],
                            ids_by_table['res_partner_address']))
        if 'res_partner' in new_vals.keys():
            ids_by_table['res_partner'] = [a.partner_id.id 
                    for a in self.pool.get('res.partner.address').browse(cr,
                            uid, ids_by_table['res_partner_address'], context)]
            netsvc.Logger().notifyChannel(self._name, netsvc.LOG_DEBUG,
                    "Get Partner IDS from Address IDS (%s): %s"
                    % (ids_by_table['res_partner_address'],
                            ids_by_table['res_partner']))
        netsvc.Logger().notifyChannel(self._name, netsvc.LOG_DEBUG,
                "calling update_replicated_fields(new_vals=%s, "
                        "ids_by_table=%s)" % (new_vals, ids_by_table))
        self.update_replicated_fields(cr, uid, new_vals, ids_by_table, context)
        return res

    # res.partner.contact
    def _prepare_replicated_vals(self, cr, uid, vals, context):
        my_rfields = self._my_replicated_fields(context)
        mod_rfields = set(vals.keys()) & set(my_rfields.keys())
        # mod_rfieldname are field which exists in vals and my_rfields
        netsvc.Logger().notifyChannel(self._name, netsvc.LOG_DEBUG,
                "fields in replicated and in vals : %s" % mod_rfields)
        if not mod_rfields:
            return {}
        mod_rtables = set(['res_partner', 'res_partner_address',
                    'res_partner_job',])
        new_vals = {}
        for mod_rfieldname in mod_rfields:
            if mod_rfieldname in self._name_fields(context):
                for table in ('res_partner', 'res_partner_address'):
                    # it forces to find partner and address IDS
                    new_vals.setdefault(table, {})['name'] = True
                continue
            newval = vals[mod_rfieldname]
            column_type = self._columns[mod_rfieldname]
            if isinstance(column_type, fields.char) and newval is False:
                newval = ''
            elif isinstance(column_type, fields.boolean):
                newval = bool(vals[mod_rfieldname])
            for table in mod_rtables:
                if table in my_rfields[mod_rfieldname]:
                    new_vals.setdefault(table, {})[mod_rfieldname] = newval
        return new_vals

    def _name_fields(self, context):
        return ('first_name', 'name')

    # res.partner.contact
    def update_replicated_fields(self, cr, uid, new_vals,
            replicate_ids_by_table, context):
        if 'name' in new_vals.get('res_partner', {}).keys():
            cr.execute("""UPDATE res_partner p
                    SET name=array_to_string(ARRAY[
                            NULLIF(c.first_name,''),
                            NULLIF(c.name,'')], ' ')
                    FROM res_partner_contact c
                        LEFT JOIN res_partner_job j ON (j.contact_id=c.id)
                        LEFT JOIN res_partner_address a ON (a.id=j.address_id)
                    WHERE p.id=a.partner_id AND p.id in %s""",
                    tuple(replicate_ids_by_table['res_partner']))
            del new_vals['res_partner']['name']
            if not new_vals['res_partner']:
                del new_vals['res_partner']
        if 'name' in new_vals.get('res_partner_address', {}).keys():
            cr.execute("""UPDATE res_partner_address a
                    SET name=array_to_string(ARRAY[
                            NULLIF(c.first_name,''),
                            NULLIF(c.name,'')], ' ')
                    FROM res_partner_contact c
                        LEFT JOIN res_partner_job j ON (j.contact_id=c.id)
                    WHERE a.id=j.address_id AND c.id in %s""",
                    tuple(replicate_ids_by_table['res_partner_address']))
            del new_vals['res_partner_address']['name']
            if not new_vals['res_partner_address']:
                del new_vals['res_partner_address']
        return update_replicated_fields(self, cr, uid, new_vals,
                replicate_ids_by_table, context)

res_partner_contact()

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

