##############################################################################
#
# Copyright (c) 2012 Guillem Barba Domingo
#                         All Rights Reserved.
#                         http://www.guillem.alcarrer.net
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

from osv import osv, fields


class create_partner_wizard(osv.osv_memory):
    _inherit = 'base.contact.create.partner.wizard'

    _columns = {
        'is_simple': fields.boolean("Is Simple?", required=True,
                help="Simple Partners normally relates to a Fisically Person "
                "and it must to have one and only one Address, Job and "
                "Contact."),
        }

    _defaults = {  
        'is_simple': True,
        }

    def create_partner(self, cr, uid, ids, data, context={}):
        res = super(create_partner_wizard, self).create_partner(cr, uid, ids,
                data, context)
        partner_id = res[0]
        form = self.browse(cr, uid, ids[0])
        self.pool.get('res.partner').write(cr, uid, partner_id, {
                'is_simple': form.is_simple,
                }, context)
        return res

create_partner_wizard()


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

