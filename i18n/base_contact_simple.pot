# Translation of OpenERP Server.
# This file contains the translation of the following modules:
#	* base_contact_simple
#
msgid ""
msgstr ""
"Project-Id-Version: OpenERP Server 6.0.3\n"
"Report-Msgid-Bugs-To: support@openerp.com\n"
"POT-Creation-Date: 2013-01-09 00:43+0000\n"
"PO-Revision-Date: 2013-01-09 00:43+0000\n"
"Last-Translator: <>\n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: \n"
"Plural-Forms: \n"

#. module: base_contact_simple
#: constraint:res.partner.contact:0
msgid "Error! You can not create two contacts with the same vat."
msgstr "Error! You can not create two contacts with the same vat."

#. module: base_contact_simple
#: code:addons/base_contact_simple/partner.py:320
#, python-format
msgid "You are trying to modify Contact's data of a Simple Partner from Partner's form.It's not recommended because it couldn't be correctly replicated.\n"
"Please, cancel the modifications and change this values in the Contact's form."
msgstr "You are trying to modify Contact's data of a Simple Partner from Partner's form.It's not recommended because it couldn't be correctly replicated.\n"
"Please, cancel the modifications and change this values in the Contact's form."

#. module: base_contact_simple
#: help:base.contact.create.partner.wizard,is_simple:0
#: help:res.partner,is_simple:0
msgid "Simple Partners normally relates to a Fisically Person and it must to have one and only one Address, Job and Contact."
msgstr "Simple Partners normally relates to a Fisically Person and it must to have one and only one Address, Job and Contact."

#. module: base_contact_simple
#: sql_constraint:res.partner:0
#: sql_constraint:res.partner.contact:0
msgid "Error! Specified VAT Number already exists for any other registered partner."
msgstr "Error! Specified VAT Number already exists for any other registered partner."

#. module: base_contact_simple
#: code:addons/base_contact_simple/partner.py:297
#, python-format
msgid "The Address with ID %(address_id)s is related to partner '%(partner_name)s' (ID:%(partner_id)s) which is a Simple Contact, but it has  %(n_jobs)s Jobs.\n"
"Simple Partners must to have one and only one Job."
msgstr "The Address with ID %(address_id)s is related to partner '%(partner_name)s' (ID:%(partner_id)s) which is a Simple Contact, but it has  %(n_jobs)s Jobs.\n"
"Simple Partners must to have one and only one Job."

#. module: base_contact_simple
#: code:addons/base_contact_simple/partner.py:87
#, python-format
msgid "The Partner '%(partner_name)s' (ID:%(partner_id)s) is a Simple Contact but it has %(n_address)s Addresses.\n"
"Simple Partners must to have one and only one Address."
msgstr "The Partner '%(partner_name)s' (ID:%(partner_id)s) is a Simple Contact but it has %(n_address)s Addresses.\n"
"Simple Partners must to have one and only one Address."

#. module: base_contact_simple
#: code:addons/base_contact_simple/partner.py:97
#, python-format
msgid "The Partner '%(partner_name)s' (ID:%(partner_id)s) is a Simple Contact but it has %(n_jobs)s Jobs.\n"
"Simple Partners must to have one and only one Job."
msgstr "The Partner '%(partner_name)s' (ID:%(partner_id)s) is a Simple Contact but it has %(n_jobs)s Jobs.\n"
"Simple Partners must to have one and only one Job."

#. module: base_contact_simple
#: code:addons/base_contact_simple/partner.py:130
#, python-format
msgid "You are trying to change the name of a Simple Partner. It's not recommended because it couldn't be correctly replicated in Contact's fields. So, it won't change the name of contact.\n"
"Please, change the name from the Contact's form."
msgstr "You are trying to change the name of a Simple Partner. It's not recommended because it couldn't be correctly replicated in Contact's fields. So, it won't change the name of contact.\n"
"Please, change the name from the Contact's form."

#. module: base_contact_simple
#: code:addons/base_contact_simple/partner.py:285
#, python-format
msgid "The Address with ID %(address_id)s is related to partner '%(partner_name)s' (ID:%(partner_id)s) which is a Simple Contact, but it has %(n_address)s Addresses.\n"
"Simple Partners must to have one and only one Address."
msgstr "The Address with ID %(address_id)s is related to partner '%(partner_name)s' (ID:%(partner_id)s) which is a Simple Contact, but it has %(n_address)s Addresses.\n"
"Simple Partners must to have one and only one Address."

#. module: base_contact_simple
#: code:addons/base_contact_simple/partner.py:493
#, python-format
msgid "Invalid Simple Contact"
msgstr "Invalid Simple Contact"

#. module: base_contact_simple
#: code:addons/base_contact_simple/partner.py:416
#, python-format
msgid "The Job ID %(job_id)s is related to partner '%(partner_name)s' (ID:%(partner_id)s) which is a Simple Contact, but it has %(n_jobs)s Jobs.\n"
"Simple Partners must to have one and only one Job."
msgstr "The Job ID %(job_id)s is related to partner '%(partner_name)s' (ID:%(partner_id)s) which is a Simple Contact, but it has %(n_jobs)s Jobs.\n"
"Simple Partners must to have one and only one Job."

#. module: base_contact_simple
#: model:ir.model,name:base_contact_simple.model_res_partner_contact
msgid "Contact"
msgstr "Contact"

#. module: base_contact_simple
#: code:addons/base_contact_simple/partner.py:118
#, python-format
msgid "Creating New Simple Partner"
msgstr "Creating New Simple Partner"

#. module: base_contact_simple
#: sql_constraint:res.partner.job:0
msgid "Unique Exception, You can only have one job per contact, address, function and state"
msgstr "Unique Exception, You can only have one job per contact, address, function and state"

#. module: base_contact_simple
#: code:addons/base_contact_simple/partner.py:86
#: code:addons/base_contact_simple/partner.py:96
#: code:addons/base_contact_simple/partner.py:284
#: code:addons/base_contact_simple/partner.py:296
#: code:addons/base_contact_simple/partner.py:415
#, python-format
msgid "Invalid Simple Partner"
msgstr "Invalid Simple Partner"

#. module: base_contact_simple
#: code:addons/base_contact_simple/partner.py:119
#, python-format
msgid "You are trying to create a new Simple Partner, but here it's not the most easiest way.\n"
"It's strongly recommended to use the 'Create Partner' wizard."
msgstr "You are trying to create a new Simple Partner, but here it's not the most easiest way.\n"
"It's strongly recommended to use the 'Create Partner' wizard."

#. module: base_contact_simple
#: code:addons/base_contact_simple/partner.py:494
#, python-format
msgid "The Contact '%(contact_name)s' (ID:%(contact_id)s) is related to a Partner which is a Simple Contact, but it has %(n_jobs)s.\n"
"Simple Contacts must to have one and only one Job."
msgstr "The Contact '%(contact_name)s' (ID:%(contact_id)s) is related to a Partner which is a Simple Contact, but it has %(n_jobs)s.\n"
"Simple Contacts must to have one and only one Job."

#. module: base_contact_simple
#: model:ir.model,name:base_contact_simple.model_res_partner_job
msgid "Contact Partner Function"
msgstr "Contact Partner Function"

#. module: base_contact_simple
#: code:addons/base_contact_simple/partner.py:319
#, python-format
msgid "Modifying Simple Partner Contacts"
msgstr "Modifying Simple Partner Contacts"

#. module: base_contact_simple
#: view:res.partner:0
msgid "Simple Contacts"
msgstr "Simple Contacts"

#. module: base_contact_simple
#: field:base.contact.create.partner.wizard,is_simple:0
#: field:res.partner,is_simple:0
#: field:res.partner.contact,is_simple:0
msgid "Is Simple?"
msgstr "Is Simple?"

#. module: base_contact_simple
#: code:addons/base_contact_simple/partner.py:129
#, python-format
msgid "Changing Simple Partner Name"
msgstr "Changing Simple Partner Name"

#. module: base_contact_simple
#: model:ir.model,name:base_contact_simple.model_res_partner
msgid "Partner"
msgstr "Partner"

#. module: base_contact_simple
#: model:ir.model,name:base_contact_simple.model_base_contact_create_partner_wizard
msgid "base.contact.create.partner.wizard"
msgstr "base.contact.create.partner.wizard"

#. module: base_contact_simple
#: model:ir.model,name:base_contact_simple.model_res_partner_address
msgid "Partner Addresses"
msgstr "Partner Addresses"

