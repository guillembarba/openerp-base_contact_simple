##############################################################################
#
# Copyright (c) 2012 Guillem Barba Domingo
#                         All Rights Reserved.
#                         http://www.guillem.alcarrer.net
#
# WARNING: This program as such is intended to be used by professional
# programmers who take the whole responsability of assessing all potential
# consequences resulting from its eventual inadequacies and bugs
# End users who are looking for a ready-to-use solution with commercial
# garantees and support are strongly adviced to contract a Free Software
# Service Company
#
# This program is Free Software; you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
##############################################################################

{
    "name" : "Base Contact Simple",
    "version" : "0.1",
    "author" : "Guillem Barba",
    "category" : "Base Contact",
    "website": "http://www.guillem.alcarrer.net",
    "description": """
    Adds new checkbox to Partner and Contact to mark these as Simple Contacts.
    A Simple Contact can only have one address and one contact. When a field
    which is in other models (for example, the e-mail) is modified, the new
    value is automatically set to other models.
    """,
    "depends" : [
        'base_contact',
        'base_contact_oneclick',
    ],
    "init_xml" : [],
    "update_xml" : [
        'partner_view.xml',
        'wizard/wizard_create_partner_view.xml',
    ],
    "demo_xml" : [],
    "active": False,
    "installable": True,
}

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:

